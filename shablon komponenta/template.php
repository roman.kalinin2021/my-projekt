<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$strRand = randString(5) . "_unique_catalog_section_list";

include($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/magnet_s1/components/bitrix/catalog.section.list/TEST/style.css');

?>



<section class="unique-catalog" id="<?=$strRand?>" data-helper="homepage::uniquecatalogsections">
    <div class="title-container" style="text-align: center;">
        <h2 class="title"><?=$arParams['BLOCK_TITLE']?></h2>
        <?php if (!empty($arParams['BLOCK_MORE_TITLE'])): ?>
            <a href="<?= $arParams['BLOCK_MORE_LINK'] ?>" class="link"><?= $arParams['BLOCK_MORE_TITLE'] ?></a>
        <?php endif; ?>
    </div>
    <div class="unique-categories">
        <?php foreach ($arResult['SECTIONS'] as $arItem): ?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>
            <div class="unique-category-wrap" id="<?php echo $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="unique-category<?=empty($arItem['DESCRIPTION']) ? ' no-description' : ''?>">
                    <div class="unique-static">
                        <?php if ($arItem['UF_DM_ICON']): ?>
                            <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="unique-img">
                                <?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($arItem['UF_DM_ICON']))?>
                            </a>
                        <?php else: ?>
                            <?php if (!empty($arItem['PICTURE']['SRC'])): ?>
                            <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="unique-img">
                                <img data-lazy="default" data-src="<?= $arItem['PICTURE']['SRC'] ?>" src="<?= SITE_TEMPLATE_PATH?>/img/no-photo.svg" alt="<?= $arItem['NAME'] ?>" title="<?= $arItem['NAME'] ?>">
                            </a>
                            <?php endif; ?>
                        <?php endif; ?>
                        <div class="unique-text">
                            <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="unique-name"><?= $arItem['NAME'] ?></a>
                            <?php if (is_array($arItem['SUB']) && !empty($arItem['SUB'])): ?>
                            <!-- тут  вывод доп ссылок -->
                            <div class="sub">
                                <? foreach ($arItem['SUB'] as $arSubItem): ?>
                                <a href="<?= $arSubItem['SECTION_PAGE_URL'] ?>" class="item"><?=$arSubItem['NAME']?></a>
                                <? endforeach; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <?php if (!empty($arItem['DESCRIPTION'])): ?>
                        <div class="unique-desc"><?= $arItem['DESCRIPTION'] ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>
