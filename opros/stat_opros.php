<?
session_start();
/*ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/
mb_internal_encoding('latin1');
//ini_set("memory_limit", "350M");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once "/home/bitrix/www/services-roznica/php-excel/PHPExcel/IOFactory.php";




$objPHPExcel = new PHPExcel();

$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();

// print_r($_GET);exit;

//колонки эксель(размер)
$sheet->getColumnDimension('A')->setWidth(20);
$sheet->getColumnDimension('B')->setWidth(20);
$sheet->getColumnDimension('C')->setWidth(50);
$sheet->getColumnDimension('D')->setWidth(20);
$sheet->getColumnDimension('E')->setWidth(20);
$sheet->getColumnDimension('F')->setWidth(20);
$sheet->getColumnDimension('G')->setWidth(20);
$sheet->getColumnDimension('H')->setWidth(20);
$sheet->getColumnDimension('I')->setWidth(20);
$sheet->getColumnDimension('J')->setWidth(20);
$sheet->getColumnDimension('K')->setWidth(20);
$sheet->getColumnDimension('L')->setWidth(20);
$sheet->getColumnDimension('M')->setWidth(20);
$sheet->getColumnDimension('N')->setWidth(20);
$sheet->getColumnDimension('O')->setWidth(20);
$sheet->getColumnDimension('P')->setWidth(20);
$sheet->getColumnDimension('Q')->setWidth(20);
//устанавливаем значения ячейкам(заголовки эксель)
$sheet->setCellValue('A1', 'ФАМИЛИЯ');
$sheet->setCellValue('B1', 'ИМЯ');
$sheet->setCellValue('C1', 'ДОЛЖНОСТЬ');
$sheet->setCellValue('D1', 'ГОРОД');
$sheet->setCellValue('E1', 'ИДЕТ/НЕ ИДЁТ');
$sheet->setCellValue('F1', 'ДАТА');
$sheet->setCellValue('G1', 'ДАТА ИЗМЕНЕНИЯ');


$filterOption = new Bitrix\Main\UI\Filter\Options('list_id');
$filterData = $filterOption->getFilter([]);



//-ЗАПРОС К БД,вытаскиваем данные из бд
$listUsers = $DB->Query("SELECT user_id, location, attendance, selected_date, last_modified 
FROM opros_korporat_2023 
");


$i=2;

// после запроса к бд запускаем цикл для добавления значений из бд в ячейки
while ($arUser=$listUsers->GetNext()){

//переписываем(из цифр в текст) город, чтобы в статистике это понятно отображалось
if ($arUser['location'] == 1 ) {
    $arUser['location'] = "Тверь";
} else {
    $arUser['location'] = "Москва";
}

//переписываем согласие
if ($arUser['attendance'] == 3 ) {
    $arUser['attendance'] = "Точно идёт";
} else {
    $arUser['attendance'] = "Точно не идёт";
}

//переписываем дату
if ($arUser['selected_date'] == 16 ) {
    $arUser['selected_date'] = "16 декабря";
} else if ($arUser['selected_date'] == 23 ) {
    $arUser['selected_date'] = "23 декабря";
} else {
    $arUser['selected_date'] = "";
}
    
 $id_for_poisk = $arUser['user_id'];

    //заполняем ячейки
    $sheet->setCellValue('D' . $i, $arUser['location']);
    $sheet->setCellValue('E' . $i, $arUser['attendance']);
    $sheet->setCellValue('F' . $i, $arUser['selected_date']);
    $sheet->setCellValue('G' . $i, $arUser['last_modified']);
    

    // Запрос к другой таблице для получения имени, фамилии и должности
    $listUserInfo = $DB->Query("SELECT NAME, LAST_NAME, WORK_POSITION FROM b_user WHERE ID = $id_for_poisk");

    if ($arUserInfo = $listUserInfo->GetNext()) {
        $sheet->setCellValue('A' . $i, $arUserInfo['LAST_NAME']);
        $sheet->setCellValue('B' . $i, $arUserInfo['NAME']);
        $sheet->setCellValue('C' . $i, $arUserInfo['WORK_POSITION']);
    }

    $i++;
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

    //$objWriter->save('php://output');
// We'll be outputting an excel file
header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=".date("d-m-Y")."-импорт данных от ТТ.xls");

//$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
ob_end_clean();
$objWriter->save('php://output');


