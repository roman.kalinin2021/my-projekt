
<?php
//индексовый файл
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Опрос");

//Получаем айдишник того кто зашел(кто авторизован) для записи в таблицу
$user_id = CUser::GetID();
// $user_id = 10;

//ищем данные в таблице для предвыбора
$sql = "SELECT location, attendance, selected_date, id FROM opros_korporat_2023 WHERE user_id = $user_id";
$result = $DB->Query($sql);
$row = $result->Fetch();

// print_r("$row");

if ($row) {
    // Если данные найдены - установить значения в поля
    $location = $row["location"];
    $attendance = $row["attendance"];
    $selected_date = $row["selected_date"];

    //выводим сообщение что уже проходил опрос
    echo '<div style="text-align: center;  margin: 20px; color: #11c4fa">Вы проходили опрос!</div>';
}

//получаем данные пост
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $location = (int)$_POST["location"]; 
    $attendance = (int)$_POST["attendance"];
    $selected_date = (int)$_POST["selected_date"];

        //вывод ошибок,если что то не заполнено color: 
        if ($location == 0) {
         echo '<div style="text-align: center; font-weight: bold; margin: 20px; color: red">Введите город.</div>';
        } elseif ($attendance == 0) { //проверка на заполнение поля город
            echo '<div style="text-align: center; font-weight: bold; margin: 20px; color: red">Выберите, пойдете ли вы.</div>';
        } elseif ($location != 0 && $attendance == 3 && $selected_date == 0) { //проверка на заполнение поля город(не равен нулю) полясогласие(Точно иду) поле дата(не выбрано)
            echo '<div style="text-align: center; font-weight: bold; margin: 20px; color: red" >Выберите дату, когда хотели бы пойти.</div>';
        } elseif ($location != 0 && $attendance !== 3 && $selected_date != 0) { //проверка на заполнение поля город(не равен нулю) полясогласие(Точно НЕ иду) поле дата(не пусто)
            echo '<div style="text-align: center; font-weight: bold; margin: 20px; color: red" >Выберите "Точно иду" чтобы голосовать за дату</div>';
        } else {
    //переменная для поля "Дата изменения"
    $now = date("Y-m-d H:i:s"); // Текущая дата и время

    // Запрос для проверки наличия записи с указанным user_id
    $strSql_select = "SELECT user_id, last_modified FROM opros_korporat_2023 WHERE user_id = $user_id";
    $res = $DB->Query($strSql_select, false, $err_mess.LINE);

// Если запись не существует, выполняем вставку
if (!$res || $res->SelectedRowsCount() == 0) {
    $strSql_insert = "INSERT INTO opros_korporat_2023 (user_id, location, attendance, selected_date, last_modified)
                      VALUES ($user_id, $location, $attendance, $selected_date, '$now')";
    $DB->Query($strSql_insert);
} else {

    // Если запись существует, выполняем обновление
    $strSql_update = "UPDATE opros_korporat_2023
                      SET location = $location, attendance = $attendance, selected_date = $selected_date, last_modified = '$now'
                      WHERE user_id = $user_id";
    $DB->Query($strSql_update);

    }

    header("Location: /oproscorp2024/?send=1");
    }
}


?>

    <style>
        .style {
            font-weight: bold;
        }
 
        h1 {
            margin-top: 20px;
        }
        .step {
            max-width: 400px;
            margin: 0 auto;
            text-align: left;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #ffffff;
        }
        
        label {
            display: block;
            margin-bottom: 10px;
        }
        select {
            width: 300px;
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        input[type="submit"],
        input[type="button"] {
            background-color: #007BFF;
            color: #fff;
            border: none;
            padding: 10px 15px;
            cursor: pointer;
            border-radius: 5px;
        }
        .all {
            width: 300px; /*  ширина в пикселях */
            margin: 0 auto; /* Устанавливает элемент по центру по горизонтали */
            border: 1px solid #ccc; /* Создает границу с цветом, по вашему выбору */
            padding: 20px; /* Добавляет внутренний отступ для создания эффекта тени */
            border-radius: 5px; /* Создает скругленные углы */
            box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1); /*  эффект тени */
            background-color: #fff; /* Устанавливает фоновый цвет*/
        }  

    </style>

<?php if($user_id == 483)  { ?>

<div class="link">
   <?php echo '<br><p><a href="stat_opros.php">Скачать статистику(Корпоратив)</a></p>'; ?>
</div>

<?php } ?>

<?php if (isset($_GET['send']) && $_GET['send'] == 1) { 
        echo "Ваш голос успешно принят.. <a href='/oproscorp2024/'>Вернуться к опросу?</a>";
        // echo "<p><a href='http://cp.trendt.ru/oproscorp2024/'>Вернуться к опросу?</a></p>";
    } else { ?>



<div class="all">
    <form method="POST" action="">
        
        <div class="step1">
            <label class="style">Откуда вы?</label>
                <select name="location">
                    <option value="0">Выберите город</option>
                    <option value="1" <?php if ($location == 1) echo 'selected'; ?>>Из Твери</option>
                    <option value="2" <?php if ($location == 2) echo 'selected'; ?>>Из Москвы</option>
                </select>

                <label class="style">Хочу поучаствовать</label>
                <select name="attendance">
                    <option value="0">Выберите пойдёте ли вы</option>
                    <option value="3" <?php if ($attendance == 3) echo 'selected'; ?>>Точно пойду</option>
                    <option value="4" <?php if ($attendance == 4) echo 'selected'; ?>>Точно не пойду</option>
                </select>
                <label class="style">Цена корпоратива: 5000 руб.(ориентировочно)</label><br>
        </div>

<?php if (isset($_POST['location']) && isset($_POST['attendance']) && $_POST['attendance'] == 3) { ?>

        <div class="step2">
             <label class="style">Дата</label>
                    <select name="selected_date">
                        <option value="0">Выберите дату</option>
                        <option value="16" <?php if ($selected_date == 16) echo 'selected'; ?>>16 декабря</option>
                        <option value="23" <?php if ($selected_date == 23) echo 'selected'; ?>>23 декабря</option>
                    </select>
        </div >
        <input type="submit" value="Отправить" id="submit" >

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
            $(document).ready(function () {
                var Data = $("select[name='selected_date']");
                var Attendance = $("select[name='attendance']");

                // Следим за полем "attendance"
                Attendance.change(function() {
                    Step2();
                    // console.log("Значение поля Хочу поучаствовать" + $(this).val());
                });

                function Step2() {
                    if (Attendance.val() === '3') {
                        $(".step2").show();
                    } else {
                        $(".step2").hide();
                        Data.val('0');
                    }
                }
            });
        </script>

<?php  } else {   ?>
        <input type="submit" value="Далее" id="submit2" >
        
    </form>
</div>

<?php  } ?>

<?php  } ?>



