<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//отправка сообщения
function sendLetter($theme, $message, $to, $from = null, $toHidden = null) {
    if (!$from) $from = 'cp@mail.ru';
    if (!$toHidden) $toHidden = '';
    $ar = array('THEME' => $theme, 'MESSAGE' => $message, 'EMAIL_TO' => $to, 'EMAIL_FROM' => $from, 'EMAIL_TO_HIDDEN' => $toHidden);
    CEvent::Send("ANT_GENERAL_LETTER", 's1', $ar, "Y");
}

$APPLICATION->SetTitle("Рассылка для опроса");

?>

    <style>

        /* Стили для чекбоксов и кнопки */
        input[type="checkbox"] {
            transform: scale(1.5);
            margin-right: 10px;
        }

        input[type="submit"] {
            padding: 10px 20px;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        button[type="button"] {
            padding: 10px 20px;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

         input[type="search"] {    
            padding:10px;
            border-radius:10px;
            width:350px;
            font-size: 15px;
        }

        input[type="email"] {
            padding:10px;
            border-radius:10px;
            width:390px;
            font-size: 15px;
        }

         input[type="text"] {
            padding:10px;
            border-radius:10px;
            width:326px;
            font-size: 15px;

        }

         input[type="url"] {
            padding:10px;
            border-radius:10px;
            width:290px;
            font-size: 15px;
        }

       

        p {
            font-size: 1.5rem;
            text-align: center;
        }

        select {
            padding: 10px;
            border-radius: 10px;
            width: 400px;
            font-size: 15px;
            background: #f6f6f6;
            border: 1px solid #cecece;
            box-shadow: 0 0 15px 4px rgba(0, 0, 0, 0.06);

        }

        /* Стили для селекта при наведении курсора */
        select:hover {
            border-color: #007bff;
        }

        /* Стили для селекта при активном состоянии (фокусе) */
        select:focus {
            border-color: #007bff;
        }

        #selected_users_ids {
            display: none;
        }
        #department {
            width: 100%;
            height: auto; 
            padding: 10px;
            border-radius: 10px;
            font-size: 15px;
            background: #f6f6f6;
            border: 1px solid #cecece;
            box-shadow: 0 0 15px 4px rgba(0, 0, 0, 0.06);
        }

        /* Стили для селекта при наведении курсора */
        #department:hover {
            border-color: #007bff;
        }

        /* Стили для селекта при активном состоянии (фокусе) */
        #department:focus {
            border-color: #007bff;
        }

        #selected_users {
            width: 100%;
            height: auto; 
            padding: 10px;
            border-radius: 10px;
            font-size: 15px;
            background: #f6f6f6;
            border: 1px solid #cecece;
            box-shadow: 0 0 15px 4px rgba(0, 0, 0, 0.06);
        }

        /* Стили для селекта при наведении курсора */
        #selected_users:hover {
            border-color: #007bff;
        }

        /* Стили для селекта при активном состоянии (фокусе) */
        #selected_users:focus {
            border-color: #007bff;
        }
        
       
    </style> 


<!-- <form method="POST" action=""  >

    <b><label for="search">Введите ФИО:</label></b>
    <input type="search" name="search" id="search">
    <button type="button" id="clearSearch">Удалить всё</button>
    <br><br>
 -->
   

       <!--  <b><label for="department">Выберите сотрудника</label></b>
        <select name="department" id="department" size="5">
            <option value=""></option>
        </select><br><br> -->

         <!-- сюда пишем айдишники тех кого нашли и кликнули... тут же селект -->
        <!-- <b><label for="selected_users">Выбранные сотрудники</label></b>
        <select name="selected_users" id="selected_users" size="5"></select>
        <textarea name="selected_users_ids" id="selected_users_ids" ></textarea><br><br>
        <br><br> -->

   <!--  <b><label for="email">Email:</label></b>
    <input type="email" name="email[]" id="email"><br><br>

    <ul class="tree" id="tree">
        <li>
            <b><input type="checkbox" name="subDepartment[]" value="568"> Управление Гребенюка Романа</b>
            <ul>
                <li><input type="checkbox" name="subDepartment[]" value="11027"> Департамент информационных технологий и безопасности</li>
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="15544"> Отдел WEB разработок </li>
                        <li><input type="checkbox" name="subDepartment[]" value="8700"> Отдел поддержки пользователей 1С </li>
                        <li><input type="checkbox" name="subDepartment[]" value="1617"> Отдел разработки информационных систем 1С </li>
                        <li><input type="checkbox" name="subDepartment[]" value="543"> Отдел поддержки пользователей и материально-технического обеспечения </li>
                    </ul>
                <li><input type="checkbox" name="subDepartment[]" value="6957"> Отдел учета рабочего времени</li>
                <li><input type="checkbox" name="subDepartment[]" value="7810"> Ревизионно-аналитический отдел</li>
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="7289"> Аналитика, подсчет итогов ревизий </li>
                        <li><input type="checkbox" name="subDepartment[]" value="4533"> Отдел видеоаналитики </li>
                        <li><input type="checkbox" name="subDepartment[]" value="10964"> Отдел видеонаблюдения </li>
                        <li><input type="checkbox" name="subDepartment[]" value="5836"> Планирование и проведение ревизий </li>
                        <li><input type="checkbox" name="subDepartment[]" value="11003"> Учет основных средств </li>
                    </ul>
            </ul>
        </li>

          <li>
            <b><input type="checkbox" name="subDepartment[]" value="17967"> Управление Антонова Михаила Константиновича</b> 
            <ul>
                <li><input type="checkbox" name="subDepartment[]" value="848"> Отдел по работе с Партнерами </li>
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="577"> Отдел по работе с Мегафон Москва </li>
                        <li><input type="checkbox" name="subDepartment[]" value="2002"> Отдел "Архив" </li>
                        <li><input type="checkbox" name="subDepartment[]" value="20357"> Отдел аналитики и поддержки продаж </li>
                        <li><input type="checkbox" name="subDepartment[]" value="10866"> Отдел по работе с Мегафон (регионы) </li>
                        <li><input type="checkbox" name="subDepartment[]" value="10868"> Отдел по работе с МТС Билайн Yota </li>
                        <li><input type="checkbox" name="subDepartment[]" value="566"> Отдел Контроля Качества </li>
                    </ul>
                <li><input type="checkbox" name="subDepartment[]" value="9639"> Отдел интернет проектов </li>
                <li><input type="checkbox" name="subDepartment[]" value="570"> Коммерческий отдел </li>
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="5246"> Отдел аналитики </li>
                        <li><input type="checkbox" name="subDepartment[]" value="7444"> Отдел закупок </li>
                        <li><input type="checkbox" name="subDepartment[]" value="2283"> Отдел оптовых продаж </li>
                        <li><input type="checkbox" name="subDepartment[]" value="3175"> Отдел маркетинга </li>
                        <li><input type="checkbox" name="subDepartment[]" value="30503"> Отдел развития </li>
                        <li><input type="checkbox" name="subDepartment[]" value="18903"> Отдел складской логистики </li>
                        <li><input type="checkbox" name="subDepartment[]" value="2703"> Отдел "Делопроизводство" </li>
                    </ul>
                <li><input type="checkbox" name="subDepartment[]" value="1389"> Отдел АХО  </li>
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="4556"> Отдел торгового оборудования </li>
                        <li><input type="checkbox" name="subDepartment[]" value="10733"> Отдел видеомонтажа </li>
                        <li><input type="checkbox" name="subDepartment[]" value="2013"> Отдел эксплуатации </li>
                    </ul>
                 <li><input type="checkbox" name="subDepartment[]" value="15088"> Дирекция по персоналу  </li>  
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="16626"> Отдел качества сервиса </li>
                        <li><input type="checkbox" name="subDepartment[]" value="560"> Учебный Центр </li>
                        <li><input type="checkbox" name="subDepartment[]" value="20746"> Call-Центр (офис) </li>
                    </ul>
                <li><input type="checkbox" name="subDepartment[]" value="10563"> Отдел кадров </li>  
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="9039"> Отдел кадров Москва </li>
                        <li><input type="checkbox" name="subDepartment[]" value="9041"> Отдел кадров Тверь </li>
                    </ul>
                <li><input type="checkbox" name="subDepartment[]" value="563"> Отдел финансовых операций  </li> -->
                <!-- <li><input type="checkbox" name="subDepartment[]" value="5971"> Отдел развития розничной сети  </li> -->
            <!-- </ul>
        </li>

          <li>
            <b><input type="checkbox" name="subDepartment[]" value="847"> Управление Антонова Михаила Константиновича</b>
            <ul>
                <li><input type="checkbox" name="subDepartment[]" value="4426"> Рестораны </li>
                <li><input type="checkbox" name="subDepartment[]" value="7205"> Служба безопасности </li>
                <li><input type="checkbox" name="subDepartment[]" value="19280"> Управление Лосева Андрея </li>
                <li><input type="checkbox" name="subDepartment[]" value="3975"> Отдел открытия </li>
            </ul>
        </li> -->

          <!-- <li>
            <b><input type="checkbox" name="subDepartment[]" value="11025"> Финансово-юридическое управление</b>
            <ul>
                <li><input type="checkbox" name="subDepartment[]" value="16334"> Юридический департамент </li>
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="564"> Московское отделение </li>
                        <li><input type="checkbox" name="subDepartment[]" value="7143"> Тверское отделение </li>
                        <li><input type="checkbox" name="subDepartment[]" value="12771"> Отдел аренды </li>
                    </ul>
                <li><input type="checkbox" name="subDepartment[]" value="11001"> Отдел финансового контроллинга </li>    
                    <ul>
                        <li><input type="checkbox" name="subDepartment[]" value="12770"> Отдел оплат </li>
                        <li><input type="checkbox" name="subDepartment[]" value="8161"> Отдел учёта торговых операций </li>
                    </ul>
                <li><input type="checkbox" name="subDepartment[]" value="634"> Бухгалтерия </li>
                <li><input type="checkbox" name="subDepartment[]" value="6726"> Отдел расчета и начисления заработной платы  </li>
                <li><input type="checkbox" name="subDepartment[]" value="19680"> Отдел учета движения денежных средств  </li>
            </ul>
        </li>
    </ul>

    <input type="submit" id="submit" value="Отправить">
    
</form> -->


<?php

echo "Форма рассылки закоментирована";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

//пустой массив для всех айдишников - в JSON
$arJSON = array();

//пустой массив для всех мейлов - в рассылку
$selectedDepartmentArray = array();

//обработка данных из ПОЛЯ ВЫБРАННЫЕ СОТРУДНИКИ
$aR_JSON_ID = array(); // Создаем пустой массив перед циклом для мейлов из текстареа
if (isset($_POST['selected_users_ids'])) {
    $selected_users_ids_str = $_POST['selected_users_ids'];

    // Разделяем строку на отдельные ID
    $selected_users_ids_array = explode(', ', $selected_users_ids_str);

    //бежим циклом по всем айдишникам 
    foreach ($selected_users_ids_array as $ID) {
        if (!empty($ID)) {
    $strSql = "SELECT EMAIL 
        FROM b_user 
        WHERE ID = $ID AND ACTIVE = 'Y'";

    $rs = $DB->Query($strSql, false, $err_mess . LINE);

    while ($resTT = $rs->GetNext()) {
        $EMAIL = $resTT['EMAIL'];
        $aR_JSON_ID[] = $EMAIL;

        }
    }


}
  $selected_users_ids_str = explode(",", $selected_users_ids_str);

   print_r($ar_ID);
    // Объединяем массивы в $selectedDepartmentArray, в 1 общий
    $selectedDepartmentArray = array_merge($selectedDepartmentArray, (array)$aR_JSON_ID);
    // print_r($selectedDepartmentArray);
}
    //обработка данных из ПОЛЯ ЕМАЙЛ
    if (isset($_POST['email']) ) { 
        $input_email = $_POST['email'];

        $input_email = implode(', ', $input_email);

        // print_r($input_email);
        $selectedDepartmentArray = array_merge($selectedDepartmentArray, (array)$input_email);
        // print_r($selectedDepartmentArray);
//ищем айдишник по емейлу при отправке почты
$strSql = "SELECT ID
            FROM b_user
            WHERE EMAIL = '" . $DB->ForSql($input_email) . "' AND ACTIVE = 'Y'";

$rs = $DB->Query($strSql, false, $err_mess . LINE);

if ($user = $rs->Fetch()) {
    $userID = $user['ID'];
    
} 


if (!is_array($userID)) {
    $userID = array($userID);
}
$arJSON = array_merge($arJSON, $userID);
// print_r($arJSON);
     }
   

     if (!is_array($selected_users_ids_str)) {
    $selected_users_ids_str = array($selected_users_ids_str);
        }
    $arJSON = array_merge($arJSON, $selected_users_ids_str);
   // print_r($arJSON);


//обработка данных из ЧЕКБОКСОВ
$arUP_Antonov = array(); // Создаем пустой массив перед циклом
if (isset($_POST['subDepartment'])) {
    $selectArID = $_POST['subDepartment'];



    foreach ($selectArID as $depId) {
        $selectArID_str = $depId; // Преобразуем каждый элемент в строку

        $strSql = "SELECT a.user_id as user_id, b.EMAIL as EMAIL
            FROM user_m2m_department a
            LEFT JOIN b_user b ON a.user_id = b.ID
            WHERE a.dep_id IN ($selectArID_str) AND b.ACTIVE = 'Y'";

        $rs = $DB->Query($strSql, false, $err_mess . LINE);

        while ($resTT = $rs->GetNext()) {
            $EMAIL = $resTT['EMAIL'];
            $ID = $resTT['user_id'];
            $arUP_Antonov[] = $EMAIL;
            $arUP_ID[] = $ID;
        }
    }

  
// print_r($arUP_ID);
    // Объединяем массивы в $selectedDepartmentArray
    $selectedDepartmentArray = array_merge($selectedDepartmentArray, $arUP_Antonov);

    if (!is_array($arUP_ID) && !empty($arUP_ID)) {
    $arUP_ID = array($arUP_ID);
    }
    // $arJSON = array_merge($arJSON, $arUP_ID);

}
print_r($arUP_ID);
// $arJSON = implode(', ', $arJSON);
// $arJSON = json_encode($arJSON);
// $arJSON = array_unique($arJSON);
// print_r($arJSON);
/////////////////////////////////////////////////
//тут записываем айдишники в файл JSON

//  $dataToEncode = array("ids" => $arJSON);
// // print_r($dataToEncode);
//     // Проверяем, существует ли файл
//     $jsonFile = 'id.php';
//     if (file_exists($jsonFile)) {
//         // Если файл существует, читаем его содержимое
//         $currentData = json_decode(file_get_contents($jsonFile), true);

//         if (isset($currentData["ids"])) {
//             // Если в файле уже есть "ids", объединяем с новыми айдишниками и убираем повторяющиеся
//             $currentData["ids"] = array_values(array_unique(array_merge($currentData["ids"], $dataToEncode["ids"])));
//         } else {
//             // Иначе создаем новый ключ "ids" в ассоциативном массиве
//             $currentData["ids"] = $dataToEncode["ids"];
//         }

//         $jsonData = json_encode($currentData);
//     } else {
//         // Если файла нет, создаем его и записываем начальные данные
//         $jsonData = json_encode($dataToEncode);
//     }

//     if (file_put_contents($jsonFile, $jsonData) === false) {
//     // echo "Не удалось записать JSON-данные в файл";
// } else {
//     // echo "JSON-данные успешно записаны в файл";
// } 


// $arJSON ="37855,1,2,3,4";


// $arJSON = implode(', ', $arJSON);
?>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

$(document).ready(function() {
    var userIDs = <?php echo json_encode($arJSON); ?>; // Передаем массив ид из PHP в JavaScript

    // Исключаем "пустые" значение из массива
    userIDs = userIDs.filter(function(userID) {
        return userID !== null && userID !== undefined && userID !== '';
    });

    // Преобразуем массив идентификаторов в строку вида ['U34836', 'U34064', 'U33685', 'U31565']
    var destString = userIDs.map(function(userID) {
        return 'U' + userID.trim();
    });





    var userID_checkbox = <?php echo json_encode($selectArID); ?>; // Передаем массив ид из PHP в JavaScript чекбоксы

     // Исключаем "пустые" значение из массива
    userID_checkbox = userID_checkbox.filter(function(userID_check) {
        return userID_check !== null && userID_check !== undefined && userID_check !== '';
    });

     // Преобразуем массив идентификаторов в строку вида ['U34836', 'U34064', 'U33685', 'U31565']
    var destString_checkBOX = userID_checkbox.map(function(userID_check) {
        return 'DR' + userID_check.trim();
    });

    //объединяем массивы с U и RD
    var allArray = destString_checkBOX.concat(destString);

    BX.rest.callMethod('log.blogpost.add', {
        POST_TITLE: 'Кто пойдет на Новогодний корпоратив',
        POST_MESSAGE: '<a href="/oproscorp2024/" style="position: fixed; top: 0; left: 0; padding: 10px; background-color: #0070c9; color: #fff; text-decoration: none; border-radius: 0 0 5px 5px;">ОПРОС</a>',
        DEST: allArray,
        USER_ID: 9158,
    }).then(function(response) {
        // Обработка ответа после создания записи в живой ленте
        if (response.error()) {
            console.log('Ошибка: ' + response.error());
        } else {
            console.log('Запись успешно добавлена в живую ленту для пользователей с идентификаторами ' + destString);
        }
    });
    console.log(destString);
    console.log(destString_checkBOX);
    console.log(allArray);
   
});
</script>
<?php

//////////////////////////////////////////////////

// Оставляем только уникальные емейлы   
$selectedDepartmentArray_array_unique = array_unique($selectedDepartmentArray);
       
//переделываем массив с емейлами в строку
$toHidden = implode(', ', $selectedDepartmentArray_array_unique);



$theme = "Кто пойдет на Новогодний корпоратив";
$mess = "$input_text_email<br>
<p>Ссылка на ОПРОС: http://cp.sait.ru/oproscorp2024/index.php </a></p>";

$from = 'cp@mail.ru'; // Адрес отправителя

$to ='';

// скрытая рассылка($toHidden)
sendLetter($theme, $mess, $to, '', $toHidden);

}

// print_r($toHidden);
 ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
    // Обработчик события "keyup" для поля ввода поиска
    $('#search').on('keyup', function() {
        var searchValue = $('#search').val();
        //если введено больше 2 символов-отправляем аякс и обрабатываем ответ
        if (searchValue.length > 2) {
            $.ajax({
                url: 'obrab_ajax.php',
                method: 'POST',
                data: { search: searchValue },
                success: function(response) {
                    if (response) {
                        var responseData = JSON.parse(response);

                        // Выбираем существующий селект
                        var select = $('#department'); 
                        select.empty();

                        for (var i = 0; i < responseData.length; i++) {
                            var user_id = responseData[i].ID; // ID пользователя
                            var full_name = responseData[i].LAST_NAME + ' ' + responseData[i].NAME + ' ' + responseData[i].SECOND_NAME;
                            var email = responseData[i].EMAIL;

                            // Создаем опцию для селекта с данными о сотруднике
                            var option = $('<option>', {
                                value: user_id,
                                text: full_name + ' (' + email + ')'
                            });

                            // Добавляем опцию в селект
                            select.append(option);
                        }
                        
                    } else {
                        console.log("Нет данных или произошла ошибка.");
                    }
                }
            });
        }
    });
//добавляем в селект "Выбранные сотрудники"
 $('#department').on('click', function() {
    var selectedUser_Id = $(this).val();
    var selectedText = $(this).find('option:selected').text();

    if (selectedUser_Id !== null && selectedUser_Id !== '') {
        // Проверяем, был ли уже выбран этот пользователь
        if ($('#selected_users option[value="' + selectedUser_Id + '"]').length === 0) {
            // Обновляем текстареа с ID только если selectedUser_Id не пустой
            var selectedUsersIds = $('#selected_users_ids').val();
            if (selectedUsersIds === '') {
                selectedUsersIds = selectedUser_Id;
            } else {
                selectedUsersIds += ', ' + selectedUser_Id;
            }
            $('#selected_users_ids').val(selectedUsersIds);

            // Создаем опцию для нового селекта
            var newOption = $('<option>', {
                value: selectedUser_Id,
                text: selectedText
            });

            // Добавляем опцию в новый селект
            $('#selected_users').append(newOption);
        }
    }
});
//удаляем из "Выбранные сотрудники"
 $('#selected_users').on('click', 'option', function() {
        var selectedUserId = $(this).val();
        if (selectedUserId) {
            // Удаляем выбранного сотрудника из селекта
            $(this).remove();

            // Удаляем выбранного сотрудника из текстареа
            var selectedUsersIds = $('#selected_users_ids').val();
            selectedUsersIds = selectedUsersIds.split(', ');
            var index = selectedUsersIds.indexOf(selectedUserId);
            if (index !== -1) {
                selectedUsersIds.splice(index, 1);
            }
            $('#selected_users_ids').val(selectedUsersIds.join(', '));
        }
    });

    // Обработчик нажатия на кнопку "Удалить всё"
    $('#clearSearch').on('click', function() {
        // Очищаем поле ввода ФИО
        $('#search').val('');

        // Очищаем селект "Выберите сотрудника"
        $('#department').empty();

        // Очищаем селект "Выбранные сотрудники"
        $('#selected_users').empty();

        // Очищаем текстареа
        $('#selected_users_ids').val('');
    });


// $('#submit').on('click', function() {
//     alert("Кнопка была нажата!");

//     var userIDs = <?php echo json_encode($arJSON); ?>; // Передаем массив ид из PHP в JavaScript
// alert(userID);
//     for (var i = 0; i < userIDs.length; i++) {
//         var userID = userIDs[i];

//         // BX.rest.callMethod('log.blogpost.add', {
//         //     POST_TITLE: 'Кто пойдет на Новогодний корпоратив',
//         //     POST_MESSAGE: '<a href="/oproscorp2024/" style="position: fixed; top: 0; left: 0; padding: 10px; background-color: #0070c9; color: #fff; text-decoration: none; border-radius: 0 0 5px 5px;">ОПРОС</a>',
//         //     DEST: ['U' + userID],
//         // }).then(function(response) {
//         //     // Обработка ответа после создания записи в живой ленте
//         //     if (response.error()) {
//         //         console.log('Ошибка: ' + response.error());
//         //     } else {
//         //         console.log('Запись успешно добавлена в живую ленту для пользователя с идентификатором ' + userID);
//         //     }
//         // });
//         alert(userID);
//     }
//     alert(userID);
// });


});

</script>


