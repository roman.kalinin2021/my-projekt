
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// Путь к файлу JSON
$jsonFile = 'id.php';

// Проверяем, существует ли файл
if (file_exists($jsonFile)) {
    // Читаем содержимое JSON-файла
    $jsonContents = file_get_contents($jsonFile);

    // Десериализация JSON
    $data = json_decode($jsonContents, true);

    if (isset($data['ids'])) {
        //$data['ids'] содержит массив айдишников
        $input_email = implode(', ', $input_email);
        print_r($data['ids']);
    } else {
        echo "Не найден в JSON-файле.";
    }
} else {
    echo "Файл не существует.";
}