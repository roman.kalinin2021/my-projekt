<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");



// Запрос к БД на основе ввода в инпут ФИО
if (isset($_POST['search'])) {
    $full_name = $_POST['search'];
    $name_parts = explode(' ', $full_name);
    if (!empty($full_name)) {  
        if (count($name_parts) > 0) {
            $strSql_search = "SELECT DISTINCT ID, NAME, LAST_NAME, SECOND_NAME, EMAIL
                             FROM b_user 
                             WHERE ACTIVE = 'Y'
                             AND (LAST_NAME LIKE '$name_parts[0]%')";

            if (count($name_parts) > 1) {
                $strSql_search = "SELECT DISTINCT ID, NAME, LAST_NAME, SECOND_NAME, EMAIL
                                 FROM b_user 
                                 WHERE ACTIVE = 'Y'
                                 AND (LAST_NAME LIKE '%$name_parts[0]' AND NAME LIKE '%$name_parts[1]%')";

                if (count($name_parts) > 2) {
                    $strSql_search = "SELECT DISTINCT ID, NAME, LAST_NAME, SECOND_NAME, EMAIL
                                     FROM b_user 
                                     WHERE ACTIVE = 'Y'
                                     AND (LAST_NAME LIKE '%$name_parts[0]' AND NAME LIKE '%$name_parts[1]' AND SECOND_NAME LIKE '%$name_parts[2]%')";
                }
            }

            $rs = $DB->Query($strSql_search, false, $err_mess . LINE);

            $options = array(); // Массив объектов для JSON-ответа

            while ($resTT = $rs->GetNext()) {
                $candidate = array(
                    'ID' => $resTT['ID'],
                    'EMAIL' => $resTT['EMAIL'],
                    'NAME' => $resTT['NAME'],
                    'LAST_NAME' => $resTT['LAST_NAME'],
                    'SECOND_NAME' => $resTT['SECOND_NAME']
                );
                $options[] = $candidate;
            }

            // Возвращаем JSON-ответ(отправляем данные о сотрудниках в формате JSON. далее переводим в формат для работы с этими данными в js)
            echo json_encode($options);
        } 
    }
}
?>
