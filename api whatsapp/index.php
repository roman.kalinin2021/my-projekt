
<!-- HTML форма для ввода номера телефона и текста сообщения -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Отправка сообщения WhatsApp</title>
</head>
<body>
    <form action="sendWhatsAppMessage.php" method="post">
        <label for="phone">Введите телефон:</label>
        <input type="text" id="phone" name="phone" placeholder="+79000000..." required>
        <label for="message">Введите сообщение:</label>
        <textarea id="message" name="message" placeholder="Hello world. Test from PHP" required></textarea>
        <button type="submit">Отправить сообщение</button>
    </form>
</body>
</html>



<?php
require_once ('vendor/autoload.php'); // Подключение автозагрузчика Composer

// Проверяем, были ли отправлены данные формы
if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['phone']) && !empty($_POST['message'])) {
    $ultramsg_token = "---"; // Ultramsg.com token
    $instance_id = "---"; // Ultramsg.com instance id
    $client = new UltraMsg\WhatsAppApi($ultramsg_token, $instance_id);

    $to = $_POST['phone']; // Получаем номер телефона из формы
    $body = $_POST['message']; // Получаем текст сообщения из формы
    $api = $client->sendChatMessage($to, $body); // Отправляем сообщение через API

    echo "<pre>";
    print_r($api); // Выводим ответ API
    echo "</pre>";
} else {
    echo "Данные формы не были отправлены.";
}
?>
